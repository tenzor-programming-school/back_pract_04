from stack import Stack

class NoBracketException(Exception):
    pass

def check_brackets(input_value):
    matching_bracket = {')': '(', '}': '{', ']': '['}

    all_brackets = set(matching_bracket.keys()) | set(matching_bracket.values())
    found_bracket = False  # если пользователь вообще не введет скобок (флаг)

    stack = Stack()

    for index, char in enumerate(input_value):
        if char in all_brackets:
            found_bracket = True
            if char in matching_bracket.values(): # если символ из строки - открывающая скобка
                stack.push(char, index + 1) # заносим в стек с позицией индекс + 1 (нумерация с единицы)

            elif char in matching_bracket: # если символ из строки - закрываюшая скобка

                if not stack: # используем __len__ из класса: если стек пуст, значит нет откр. скобки
                    return index + 1 # возвращаем позицию несуществующей откр. скобки
            
                else: # есть открывающая скобка
                    top = stack.pop() # возврат откр.скобки
                    # если откр. скобка из словаря не соответствует откр. скобке из стека
                    if matching_bracket[char] != top:
                        return index + 1

    if not found_bracket:  # Если не найдено ни одной скобки
        raise NoBracketException("Строка не содержит скобок!")            
    # проверка строки прошла, а символы в стеке остались - возвращаем позицию первой незакр. скобки
    if not stack.is_empty():
        return stack.top_position()

    return "Success"


def main():
    input_string = input("Введите строку: ")
    result = check_brackets(input_string)
    print(result) 


if __name__ == "__main__":
    main()