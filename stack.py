class Stack:
    def __init__(self):
        self.items = []
        self.positions = [] # добавляем для удобства список позиций (индексов) открывающих скобок

    # проверяем есть ли что-то в стеке
    def is_empty(self):
        return not self.items

    # добавление элемента в стек
    def push(self, item, position):
        self.items.append(item)
        self.positions.append(position)
    
    # достаём верхний элемент стека
    def pop(self):
        if not self.is_empty():
            self.positions.pop()
            return self.items.pop()
        return None
        
    # заглянуть в стек и понять какой элемент является последним
    def peek(self):
        if not self.is_empty():
            return self.items[-1]
        return None
    
    # получить размер стека
    def __len__(self):
        return len(self.items)
    
    # получить позицию верхней открывающей скобки
    def top_position(self):
        if not self.is_empty():
            return self.positions[-1]
        return None